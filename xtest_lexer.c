#include <stdio.h>

#include "lexer.h"

int
main(void)
{
    int token;
    while ((token = getToken()) != 0) {
        printf("%zu.%zu: ", token_line, token_col);
        if (token == 1) {
            printf("BAD_TOKEN\n");
        } else if (token == 2) {
            printf("HEX_LITERAL\n");
        } else if (token == 3) {
            printf("OCT_LITERAL\n");
        } else if (token == 4) {
            printf("DEC_LITERAL\n");
        } else if (token == 5) {
            printf("PLUS\n");
        } else if (token == 6) {
            printf("MINUS\n");
        } else if (token == 7) {
            printf("ASTERISK\n");
        } else if (token == 8) {
            printf("SLASH\n");
        } else if (token == 9) {
            printf("PERCENT\n");
        } else if (token == 10) {
            printf("EQUAL\n");
        } else if (token == 11) {
            printf("LPAREN\n");
        } else if (token == 12) {
            printf("RPAREN\n");
        } else if (token == 13) {
            printf("SEMICOLON\n");
        } else if (token == 14) {
            printf("IDENTIFIER\n");
        } else {
            printf("?? internal error ?? token = %d\n", token);
        }
    }
}
