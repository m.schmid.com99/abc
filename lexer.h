#ifndef ABC_LEXER_H
#define ABC_LEXER_H

#include <stddef.h>

/*
   Token kind:

    0 = EOI (end of input)
    1 = BAD_TOKEN
    2 = HEX_LITERAL
    3 = OCT_LITERAL
    4 = DEC_LITERAL
    5 = PLUS ('+')
    6 = MINUS ('-')
    7 = ASTERISK ('*')
    8 = SLASH ('/')
    9 = PERCENT ('%')
   10 = EQUAL ('=')
   11 = LPAREN (left paranthesis '(')
   12 = RPAREN (right paranthesis ')')
   13 = SEMICOLON
   14 = IDENTIFIER
*/

int getToken(void);

extern int token_kind;
extern size_t token_line;
extern size_t token_col;


#endif // ABC_LEXER_H
